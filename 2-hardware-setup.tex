\section{Hardware Setup}\label{sec:hardware}

\subsection{Overview}

The ATLAS DCS consists of two components, the Supervisory Control And Data Acquisition (SCADA) system and the Front-End I/O (FEIO) system. The aim is to have an as homogeneous system as possible for all subdetectors. On the SCADA side this is guaranteed by using the same commercial software system throughout. The connection of the SCADA to the FEIO will be achieved by a limited number of standards such as CAN fieldbus, OPC software etc. The FEIO is the responsibility of the subdetector group, but a versatile general-purpose system, the Local Monitor Box (LMB) has been designed and built and is now widely accepted by the subdetector groups. 

After successful tests of the LMB and feedback from the subdetector groups a new version, the Embedded Local Monitor Board (ELMB) has been designed.  In 2002, the ELMB’s main microprocessor was changed from an AVR ATmega103L to a newer, chip-compatible version, the ATmega128L and so became the ELMB128. It has many more functions as compared to the LMB and its packaging follows the subdetectors needs. The main differences are, that the ELMB128 comes in the form factor of a credit card sized piggy board and that it has many digital I/O lines which can be fully programmed by the (advanced) user. The ELMBio firmware for the ELMB is considered the standard ELMB firmware. It provides access to all regular I/O blocks of the device. There are some non-standard firmware binaries in use that however should serve only special and exotic use cases.  As an option the ELMB128 comprises a multiplexed 64-channel ADC with 16-bit resolution (and a 7-bit dynamic range, from 25 mV to 5 V) that can be used from the SCADA system without dedicated programming. The board can either be directly plugged onto the subdetector front-end electronics, or onto a general-purpose motherboard, which adapts the I/O signals. 

The environmental requirements are essentially unchanged. It should be usable in USA15 outside of the calorimeter in the area of the MDTs and further out. This implies tolerance (with safety factors) to radiation up to about 5 Gy and 3·10$^{10}$ neutrons/cm$^2$ for a period of 10 years and to a magnetic field up to 1.5 T.


\subsection{Hardware Installation \& Configuration}

% \subsubsection{Kvaser PCI CAN Card Interface}

% The Kvaser CAN cards are available in either 4-port or 2-port format for PCI cards, and PCMCIA cards of either 1-port or 2-ports are available for laptops.

% % \begin{figure}[h]
% %     \centering
% %     \includegraphics[width=0.5\textwidth]{fig/kvaser-conf.png}
% %     \caption{Window showing the Kvaser CAN interface card configuration}
% %     \label{fig:kvaser-conf}
% % \end{figure}

% The installation procedure for the Kvaser cards is described in the Kvaser documentation available from the Kvaser web site\footnote{\url{www.kvaser.com}}.  The configuration for the card is available through the ‘Control Panel’ under the item ‘CAN Hardware’, as shown in Figure \ref{fig:kvaser-conf} for a PC with a single 4-port card installed.  The default configuration assigned on installation should not need to be altered.  It should be noted that although the dialog shows the channels (ports) assigned the numbers 1 to 4, the ‘names’ of these ports are ‘0’ to ‘3’.

% \vspace{1em}
% \textit{NOTE: The Kvaser 4-port PCI card (PCIcan-Q) has some switches (referred to as SW-2 in the documentation) that are set by default to connect all four buses to a ‘common bus’.  Please refer to the Kvaser documentation for more information and instructions on obtaining the required setting for your application.}


\subsubsection{SocketCAN on Linux}

\paragraph{SysTec}

ATLAS DCS delivers RPM package with the driver and additional tools for SysTec. List of example tools and installation procedure are in section \ref{sec:setup-verification}.

Drivers for Windows are available from the SysTec web site\footnote{\url{http://www.systec-electronic.com/en/home/support/download-area/drivers}}.


\paragraph{PCAN}

In ATLAS PCAN\footnote{\url{http://www.peak-system.com/PCAN-USB.199.0.html?&L=1}} is rarely used device however RPM package might be available from Piotr Nikiel\footnote{\url{piotr.nikiel@cern.ch}}, Central DCS.


\subsubsection{Motherboard}

The different power schemes of the ELMB128 are explained in Appendix \ref{sec:annex-b}, ELMB Specification.
Different types of powering are possible:
\begin{itemize}
\vspace{-0.5em}
\setlength\itemsep{-0.25em}
    \item A power supply with at least one 10 V output (200 mA)\footnote{Although this configuration is possible, it is not recommended as ground loops may occur on a long bus.}
    \item A power supply with two outputs, each 10 V (see Figure \ref{fig:motherboard})
    \item A power supply with three outputs (each one giving 10 V)
\end{itemize}

\vspace{1em}
\textit{NOTE: There should be one 120 ohm termination at each end of the CANbus cable.} 

\begin{figure}[h]
    \centering
    \includegraphics[width=\textwidth]{fig/motherboard.png}
    \caption{Power connection of the ELMB Motherboard}
    \label{fig:motherboard}
\end{figure}


\subsubsection{Adapters}

On the backside of the motherboard there are spaces for 16 sockets for dual-in-line signal adapters, each servicing 4 input channels. There are presently adapters for 4-wire Pt100 sensors, 2-wire resistive sensors and differential voltage attenuators (1:100). The ADC voltage reference (+2.5 V) and the analog ground are available on each adapter. Different types of adapters may be mixed, however it is required that the same ADC range should be used for all of them. In addition common resistor networks (providing 1 k$\mathrm{\Omega}$ resisters in series) may be used in the sockets for the direct connections to the onboard multiplexer and ADC.

For details and technical information about the adapters, see Appendix \ref{sec:annex-c}, Adapters Specification.


\subsubsection{ELMB128}

The ELMB128 is a general-purpose plug-in board. It is based on an AVR microcontroller ATmega128L.  The CAN controller is based on a SAE81C91. A galvanic isolation to the CAN bus is made with fast optocouplers between the CAN bus transceiver PCA82C251 and protocol chip. There is a DIP-switch for the baud rate and the CAN identifier.

Using the onboard DIP-switches a node identifier must be set between 1 and 63 (unique on the bus), using 6 of the 8 switches, and a CAN-bus bit rate of 50, 125, 250 or 500 kbit/s, using the 2 remaining switches. See Figure \ref{fig:dip-switch} below for details.

\begin{figure}[h]
    \centering
    \includegraphics[width=\textwidth]{fig/dip-switch.png}
    \caption{Location and function of ELMB128 DIP-switches and the 10-pin Programmer/RS232 adapter connector}
    \label{fig:dip-switch}
\end{figure}

Three low-drop power regulators are used as filters and with current limitation for the different voltages needed. All of these components are mounted on a PCB of the size 50x66 mm. On the backside of this PCB are two high-density connectors of SMD type and optionally a high-performance 16+7 bit delta-sigma ADC with 64 differential inputs. There are also analog power regulators for the supply of the ADC.

\newpage