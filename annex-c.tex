\subsection{Adapters specification}\label{sec:annex-c}

% High performance Pt100 adapter
%\subsection*{\normalsize High performance Pt100 adapter}
\subsubsection{High Performance Pt100 Adapter}

Four-wire connection to the sensor eliminates the voltage drop in the wires, see Figure \ref{fig:4w}. Two channels of the ADC are used. The sensor resistance is given by the ratio of the ADC readings for ch1 and ch0 times value of the RS resistor. Therefore the performance is essentially given by the quality of the resistor RS. A high stability type is recommend.  Typical values are for 3.9 k$\mathrm{\Omega}$ for RC and 100 $\mathrm{\Omega}$ (0.1\%) for RS. The resistor RC determines the current through the sensor. It should be scaled such that the full-scale range of the ADC for the full temperature range required is below 100 mV and that the input current of the ADC can be neglected ($\sim$100 pA). Calibration has to be done by exchanging the sensor with a known stable high-precision resistor. The motherboard has place for four adapters of the type shown in Figure \ref{fig:4w-adapter} per 16 channel inputs.

\FloatBarrier

\begin{figure}
\begin{minipage}[h]{0.48\textwidth}
    \centering
    \captionsetup{justification=centering,margin=0.1cm}
    \includegraphics[width=\textwidth]{fig/4w.png}
    \captionof{figure}{Principle of the 4-wire resistance measurement}
    \label{fig:4w}
\end{minipage}
~
\begin{minipage}[h]{0.48\textwidth}
\captionsetup{justification=centering,margin=0.1cm}
    \centering
    \includegraphics[width=0.5\textwidth]{fig/4w-adapter.png}
    \captionof{figure}{Plug-in adapter for 2~channels}
    \label{fig:4w-adapter}
\end{minipage}
\end{figure}


The equation to be used for the sensor comes from the equation below:
\[R(t)=R_0(1+at+bt^2)\]
This can be solved to give the value for the temperature $t$, from the equation:
\[t=\frac{-a+\sqrt{a^2-4b(1-R(t)/R_0)}}{2b}\]
The values of $a$ and $b$ are given by the manufacturer of the sensor. Typical values for these constants are $a = 3.9083\cdot10^{-3}$ and $b = -5.775\cdot10^{-7}$. $R_0$ is the value of the resistor at 0$^{\circ}$C (for a Pt100, this is 100). $R(t)$ is the resistance of the sensor at the temperature being measured.

\FloatBarrier

The equation for $R(t)$ is given by:
\[R(t)=\frac{\mathit{ch1}\cdot R_S}{\mathit{ch0}},\]
where:
\begin{itemize}
\vspace{-0.5em}
\setlength\itemsep{-0.25em}
    \item[-] $ch1$ is the value read by channel~1,
    \item[-] $ch0$ is the value read by channel~0 (as shown in Figure~\ref{fig:4w}).
\end{itemize}

For the 4-wire sensors, it is not important whether the values read are in $\mathrm{\mu}$V or in ADC counts, as the ratio of the two channels is taken.

\FloatBarrier

% Resistance Temperature Detector (RTD) sensors
%\subsection*{\normalsize Resistance Temperature Detector (RTD) sensors}
\subsubsection{Resistance Temperature Detector (RTD) Sensors}

RTD sensors (for example NTC 10k or Pt1000) but also other sensors like strain gauges and position sensors where the resistance changes as function of the parameter can be measured with this adapter. The principle of 2-wire measurements of resistive sensors is shown in Figure \ref{fig:2w}. The resistance of the connection wire will influence the accuracy of the measurements but this effect can be reduced by calibration. The input current of the ADC has also to be taken into account. The circuit should be calibrated by replacing the sensor with a known precision resistor. About 10 mA per input connector (16 channels) is available from the $V_{ref}$ in Figure \ref{fig:2w}. The $V_{ref}$ is generated with the help of a stable precision operational amplifier from the same reference voltage as is used by the ADC. The adapter is shown in Figure \ref{fig:2w-adapter}. The value of the resistors in Figure \ref{fig:2w-adapter} for 10 k$\mathrm{\Omega}$ at 25$^{\circ}$C NTC resistors is chosen to be 1 M$\mathrm{\Omega}$. This permits measurements of temperatures in the range from -5$^{\circ}$C to $>$100$^{\circ}$C at a constant ADC input voltage range of 100 mV.

\begin{figure}
\begin{minipage}[h]{0.48\textwidth}
\captionsetup{justification=centering,margin=0.1cm}
    \centering
    \includegraphics[width=\textwidth]{fig/2w.png}
    \captionof{figure}{Principle of the 2-wire measurements}
    \label{fig:2w}
\end{minipage}
~
\begin{minipage}[h]{0.48\textwidth}
\captionsetup{justification=centering,margin=0.1cm}
    \centering
    \includegraphics[width=0.5\textwidth]{fig/2w-adapter.png}
    \captionof{figure}{Plug-in adapter for 4~channels}
    \label{fig:2w-adapter}
\end{minipage}
\end{figure}

The equation for the 2-wire Pt sensor is the same as for the 4-wire sensor.  However, the calculation for the resistance $R(t)$ is different and is given by:
\[R(t)=\frac{\mathit{ch0}\cdot R_A}{2.5-\mathit{ch0}},\]
where:
\begin{itemize}
\vspace{-0.5em}
\setlength\itemsep{-0.25em}
    \item[-] \textit{ch0} is the voltage measured by the channel,
    \item[-] $R_A$ is the value of the resistor on the adapter.
\end{itemize}

\FloatBarrier

If the ELMB has been set to give ADC counts (and not $\mathrm{\mu}$V) or is an older ELMB that does not have the ability to send values in $\mathrm{\mu}$V then the value from the channel reading must be converted to volts by the following formula:
\[\mathit{ch0}_{\mathrm{volts}}=\frac{\mathit{ch0}_{\mathrm{counts}}\cdot Range_{\mathrm{volts}}}{65535},\]
where:
\begin{itemize}
\vspace{-0.5em}
\setlength\itemsep{-0.25em}
    \item[-] \textit{ch0}$_{volts}$ is the result value in volts,
    \item[-] \textit{ch0}$_{counts}$ is the ADC count as returned by the ELMB for the channel,
    \item[-] $Range_{volts}$ is the currently set voltage range for the ELMB’s ADC (e.g. for 100mV, this is 0.1).
\end{itemize}
	
\noindent For an NTC sensor, the equation is given below:
\[T=\frac{1}{A+B\cdot \ln(R(t))+C\cdot (\ln(R(t)))^{3}}\]
The values of $A$, $B$ and $C$ are given by the manufacturer of the sensor. Typical values for these constants are $A = 9.577\cdot10^{-4}$, $B = 2.404\cdot10^{-4}$ and $C = 2.341\cdot10^{-7}$. $R(t)$ is the resistance of the sensor at the temperature being measured as calculated above for the 2-wire Pt sensor.

\FloatBarrier
% Differential attenuator
\subsubsection{Differential Attenuator}

The Crystal Semiconductor ADC CS5523 used in the ELMB with the input multiplexer can measure voltages up to the range from -2 V to 5 V. The common mode range is -0.15 V to 0.95 V on the three lowest voltage ranges and -2 V to 5 V in the other ranges. With the help of a differential attenuator the input ranges of the ADC can be extended see Figure \ref{fig:diff}. The ratios of $R1$ to $R2$ and $R3$ to $R4$ should be matched to the wanted range. The value of the resistors should be chosen so that ground loop currents can be neglected ($>$100 k$\mathrm{\Omega}$). The adapter is shown in Figure \ref{fig:diff-adapter}. Typical values are 1 k$\mathrm{\Omega}$ and 100 k$\mathrm{\Omega}$ for a 1:100 divider.

\begin{figure}
\begin{minipage}[b]{0.48\textwidth}
\captionsetup{justification=centering,margin=0.1cm}
    \centering
    \includegraphics[width=\textwidth]{fig/diff.png}
    \captionof{figure}{Principle of the differential attenuator}
    \label{fig:diff}
\end{minipage}
~
\begin{minipage}[b]{0.48\textwidth}
\captionsetup{justification=centering,margin=0.1cm}
    \centering
    \includegraphics[width=0.5\textwidth]{fig/diff-adapter.png}
    \captionof{figure}{Plug-in adapter with differential attenuators}
    \label{fig:diff-adapter}
\end{minipage}
\end{figure}

\FloatBarrier
% Resistor network
\subsubsection{Resistor Network}

A standard 16-pin dual-in-line resistor network can be used, see Figure \ref{fig:res-adapter}. This connects the external connector directly to the analog multiplexers of the ADC. It should be noted that the input voltage range must be limited (between –2 V and 5 V) because the input at the ELMB is not protected against over voltages! Also the common mode range is limited depending on the ADC range used. To ensure that the bias/leakage currents of the input multiplexer and ADC do not influence the measurements, resistors of value less than 1 k$\mathrm{\Omega}$ should be used. The typical adapter supplied with the ELMB motherboard consists of 1 k$\mathrm{\Omega}$ resistors.

\begin{figure}[h]
    \centering
    \includegraphics[width=0.2\textwidth]{fig/res-adapter.png}
    \caption{Plug-in adapter with resistor network}
    \label{fig:res-adapter}
\end{figure}

\FloatBarrier
% Extraction Tool
\subsubsection*{Extraction Tool}
To remove the adapters from the motherboard, we recommend using the~Integrated Circuit Extraction Tool from CERN stores\footnote{Reference number: 34.95.05.310.7}.

\newpage